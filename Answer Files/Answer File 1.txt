2. 
I changed the directory using cd to get to the file I created 

3. 
For the name
git config --global user.name "Samo"
For the email
git config --global user.email "samanthagomplewicz@gmail.com"

Configuration file
input: 
git config --list

output:
credential.helper=osxkeychain
user.name=Samo
user.email=samanthagomplewicz@gmail.com

4. I was already in my directory
git init 

Show the hidden files
input:
ls -a

output:
.                   .git                Git cheat sheet.pdf
..                  Answer File 1

5.
Create a file and put text in it
input:
touch hello_world.txt
echo "Lorem   ipsum   dolor   sit   amet, consectetuer   adipiscing   elit. Aenean   commodo   ligula   eget   dolor. Aenean   massa. 
Cum   sociis   natoque   penatibus   et   magnis   dis   parturient   montes, nascetur   ridiculus   mus.
Donec   quam   felis,   ultricies   nec,   pellentesque   eu,   pretium   quis,   sem. Nulla   consequat   massa   quis   enim.
Donec   pede   justo,   fringilla   vel,   aliquet   nec,   vulputate   eget,   arcu." > hello_world.txt

input: 
git status

output:
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	Answer File 1
	Git cheat sheet.pdf
	hello_world.txt

nothing added to commit but untracked files present (use "git add" to track)

Add the file to the repository
input:
git add hello_world.txt 
git status

output:
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

	new file:   hello_world.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	Answer File 1
	Git cheat sheet.pdf

The staged file is just added to the local repository but the changes have not been saved on the local repository

Remove the file from the staging area
input:
git reset hello_world.txt
git status

output:
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	Answer File 1
	Git cheat sheet.pdf
	hello_world.txt

nothing added to commit but untracked files present (use "git add" to track)

Create a new file with creative content:
touch Git_first_file.txt
echo "bisous" > Git_first_file.txt

Add all the files in the repository:
git add --all
git status

output:
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

	new file:   Answer File 1
	new file:   Git cheat sheet.pdf
	new file:   Git_first_file.txt
	new file:   hello_world.txt


6. First Commit
input:
git commit --all -m "First Commit"

output:
[master (root-commit) 112bbbb] First Commit
 4 files changed, 122 insertions(+)
 create mode 100644 Answer File 1
 create mode 100644 Git cheat sheet.pdf
 create mode 100644 Git_first_file.txt
 create mode 100644 hello_world.txt


 Inspect the commits:
input:
git log

output:
 commit 112bbbb87b01bd5fa52ddbe8883e5f7b5d957c96 (HEAD -> master)
Author: Samo <samanthagomplewicz@gmail.com>
Date:   Thu May 2 11:22:39 2019 +0300

    First Commit
(END)
