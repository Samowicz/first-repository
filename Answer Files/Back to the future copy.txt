1. 
vim hello_world.txt
i to get in insertion mode
I deleted a word in middle of the text
echap, :x to save and quit

git status


output:
On branch master
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)
	modified:   hello_world.txt


Inspects   the   difference   between   the   last   committed version   and   the current   modified   version
input:
git diff

output:
shows in red what has been modified (and is not the same) and shows in green what is modified and currently in the file



2. Reset the changes to the last commit
input:
git reset —hard hello_world.txt
git status 
To check if it was reset

3. Time travelling

edit hello_world:
vim hello_world.txt
i for insertion mode
added a word
esc, :x

git add hello_world.txt
git commit -m "added word" hello_world.txt

output: can see the 2 last commit
[master a969425] added word
 1 file changed, 2 insertions(+), 2 deletions(-)

 inspect log of commit:
 git log

 output:
commit 27d314b58a7892186c8f29abd1da46800b68f438 (HEAD -> master)
Author: Samo <samanthagomplewicz@gmail.com>
Date:   Thu May 2 12:06:06 2019 +0300

    added word

commit 4a3b6abf567774fb1c3c7c15c50e3f4bf6542176
Author: Samo <samanthagomplewicz@gmail.com>
Date:   Thu May 2 12:04:11 2019 +0300

    First commit

rollback to first commit:
git checkout 4a3b6abf567774fb1c3c7c15c50e3f4bf6542176

output:
You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by performing another checkout.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -b with the checkout command again. Example:

  git checkout -b <new-branch-name>

HEAD is now at 4a3b6ab... First commit

input:
git status

output:
HEAD detached at 4a3b6ab

git diff master
display the changes we made in 2 different colors: red what left, green, what's added.

To go back to last commit:
git checkout master
output:
Previous HEAD position was 4a3b6ab... First commit
Switched to branch 'master'

git status
output:
On branch master




  

